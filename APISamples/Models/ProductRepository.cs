﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APISamples.Models
{
    public class ProductRepository : IProductRepository
    {
        private List<Product> products = new List<Product>();
        private int _nextId = 1;

        private static ProductRepository instance = null;
        private static readonly object padlock = new object();

        private ProductRepository()
        {
            for (int i = 0; i < 1000; i++)
            {
                Add(new Product { Name = $"Producto #{i + 1}", Category = $"Categoria #{i + 1}", Price = 1M * (i + 1) });
            }
        }

        public static ProductRepository Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                        instance = new ProductRepository();

                    return instance;
                }
            }
        }

        public IEnumerable<Product> GetAll()
        {
            return products;
        }

        public Product Get(int id)
        {
            return products.Find(p => p.Id == id);
        }

        public Product Add(Product item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            item.Id = _nextId++;
            products.Add(item);
            return item;
        }

        public void Remove(int id)
        {
            products.RemoveAll(p => p.Id == id);
        }

        public bool Update(Product item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            int index = products.FindIndex(p => p.Id == item.Id);
            if (index == -1)
            {
                return false;
            }
            products.RemoveAt(index);
            products.Add(item);
            return true;
        }
    }
}