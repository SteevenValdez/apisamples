﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Routing;
using APISamples.Models;
using Microsoft.Data.OData;

namespace APISamples.Controllers
{
    /*
    Puede que la clase WebApiConfig requiera cambios adicionales para agregar una ruta para este controlador. Combine estas instrucciones en el método Register de la clase WebApiConfig según corresponda. Tenga en cuenta que las direcciones URL de OData distinguen mayúsculas de minúsculas.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using APISamples.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Product>("Products");
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class ProductsController : ODataController
    {
        static readonly IProductRepository repository = ProductRepository.Instance;
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        // GET: odata/Products
        public async Task<IHttpActionResult> GetProducts(ODataQueryOptions<Product> queryOptions)
        {
            // validate the query.
            try
            {
                queryOptions.Validate(_validationSettings);
            }
            catch (ODataException ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok<IEnumerable<Product>>(repository.GetAll());
            //return StatusCode(HttpStatusCode.NotImplemented);
        }

        // GET: odata/Products(5)
        public async Task<IHttpActionResult> GetProduct([FromODataUri] int key, ODataQueryOptions<Product> queryOptions)
        {
            // validate the query.
            try
            {
                queryOptions.Validate(_validationSettings);
            }
            catch (ODataException ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok<Product>(repository.Get(key));
            //return StatusCode(HttpStatusCode.NotImplemented);
        }

        // PUT: odata/Products(5)
        //public async Task<IHttpActionResult> Put([FromODataUri] int key, Delta<Product> delta)
        //{
        //    Validate(delta.GetEntity());

        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    // TODO: Get the entity here.

        //    // delta.Put(product);

        //    // TODO: Save the patched entity.

        //    return Updated(repository.Update(delta.GetEntity()));
        //    return StatusCode(HttpStatusCode.NotImplemented);
        //}

        // POST: odata/Products

        public async Task<IHttpActionResult> Post(Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // TODO: Add create logic here.

            return Created(repository.Add(product));
            //return StatusCode(HttpStatusCode.NotImplemented);
        }

        // PATCH: odata/Products(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<Product> delta)
        {
            Validate(delta.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var productEntity = repository.Get(key);
            if (productEntity == null) throw new HttpResponseException(HttpStatusCode.NotFound);

            delta.Patch(productEntity);

            // TODO: Get the entity here.

            // delta.Patch(product);

            // TODO: Save the patched entity.

            return StatusCode(HttpStatusCode.OK);
            //return Updated(repository.Update(delta.GetEntity()));
            //return StatusCode(HttpStatusCode.NotImplemented);
        }

        // DELETE: odata/Products(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            repository.Remove(key);
            // TODO: Add delete logic here.
            return StatusCode(HttpStatusCode.OK);
            //return StatusCode(HttpStatusCode.NoContent);
            //return StatusCode(HttpStatusCode.NotImplemented);
        }
    }
}
